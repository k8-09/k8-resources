### Storage in Kubernetes
By default pods and containers are ephemeral. Once pods and containers are terminated we lose the data as well. To persist the data Kubernetes offers below storage solutions.

![alt text](storage.jpg)

* emptyDir
* hostPath
* ExternalStorage
    * Static
    * Dynamic

### emptyDir
* emptyDir will be created inside the node where the pod is running. 
* Location will be /var/lib/kubelet/pods/*. 
* We can attach emptyDir to the pods through volume and volumeMount. 
* But the data will be deleted once the pod is terminated. 
* We use this strategy in side car containers where we push logs to external systems like ELK.

### hostPath
* We create the directory inside the node and attach to pod.
* Data will be persisted even we lose the pod.
* But if the pod runs in another node it can't access hostPath volume.
* As long as pod schedules in the same node it can access hostPath directory.

#### emptyDir and hostPath are related to internal storage, to extract the storage out of kubernetes cluster kubernetes come up with PV, PVC and StorageClass option.

### External Storage
* There are many kind of storages available in the market like NFS, iSCSI or Cloud provided storage.
* Kubernetes come up with a concept called PV and PVC to abstract the storage responsibilities from the users or admins.

### Static Provisioning
* PV, PVC concepts are used static provisioning.
* For example storage team will create 50GB NFS storage outside of Kubernetes cluster and provide the details to users.
* Users share those details to Kubernetes admins, then admin creates a resource called Persistent Volume(PV).
* PV is the resource inside Kubernetes cluster representing the external storage.
* Now pods need to claim those PV using Persistent Volume Claim(PV).
* PVC is a namespace level resource where as PV is cluster level resource.
* PV is physical storage PVC is logical.

![alt text](static.png)

### Dynamic Provisioning
* Instead of storage team creating the storage manually we can make it automate through Storage Class.
* We need to write a Storage Class object that is authorized to create external storage on behalf of us.
* Here PVC and Storage Class work together.
* PVC will create Storage and PV automatically using Storage Class.

![alt text](dynamic.jpg)

### EBS vc EFS
* EBS we use for stateful applications where one pod access the data.
* EFS we use for stateless applications where many pods in multiple nodes can access data

