### Affinity
By default it is up-to scheduler where to schedule the pod. Some cases we want to schedule our pods on particular nodes for security, low latency or some other reasons.
##### Nodeselector is the solution to select the node based on labels. #####

How to label the node
```
kubectl label node <node-name> <key>=<value>
```
Once you label the node you can use below syntax for scheduling.

```
nodeSelector:
    tier: db
```

* What if the labels doesn't match? Pod will never come to Running state
* What if you want opposite, you want pod not to run on particular node.

### Node Affinity
to address the above problems we have nodeAffinity. We have two approaches here
1. requiredDuringSchedulingIgnoredDuringExecution <br/>
    It is a hard rule, pod will be scheduled if the labels match only.
2. preferredDuringSchedulingIgnoredDuringExecution <br/>
    It is soft rule, scheduler will try to match if it is not success, it will schedule in another node.

To achieve node anti affinity we need to use NotIN operator. See the examples

### Pod Affinity
Similar to nodeAffinity there is a concept of podAffinity and podAntiAffinity. Understand the below scenario. <br/>
Reference: https://www.densify.com/kubernetes-autoscaling/kubernetes-affinity

* We want DB related or some stateful apps to be scheduled in a particular node. You can attach disk to that node for storage purpose.
* You have an app that cache the data from DB into some cache server like redis.
* You want actual app that hits the cache first to get the data, if it is not found there it can hit DB.

#### Solution
We create application pod in each node and cache pod in the same node for low latency. So each node should have one application pod and cache pod, 2 application pods or 2 cache pods should not be in same node.

![alt text](affinity.jpg)