### Service

#### Problem
* Pods are ephemeral, their network identity keeps on changing. 
* There can be many replicas of pods running as well
* APP-A pod needs to communicate with APP-B pod, using IP address will not exactly work because IP keeps changing.
* If many replicas are running balancing the requests is a problem.

Kubernetes implemented service discovery mechanism using Service.
* Many pods can attach themselves to service using selector labels. Then service can work as a Load Balancer.
* Pods can reach other Pods using service names that is same all the time.
* Usually every pod will have one service attached.

![alt text](service.jpg)