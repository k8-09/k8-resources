### POD
* Pod is a smallest deployable unit in k8
* It can contain multiple containers
* Network and Storage can be shared across containers, this is the main difference between normal container and Pod