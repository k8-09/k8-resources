### What is ReplicaSet
* ReplicaSet makes sure the declared no of pods running all the time.
* It maintains the pods based on selector labels.
#### Drawback
* When you need new version of app ReplicaSet can't do it.

### Deployment
* Make sure desired no of pods are always running through ReplicaSet.
* It is one step higher than ReplicaSet to upgrade the applications which ReplicaSet can't do.
* You can control the Deployment rate using the strategy like RollingUpdate.
* Deployment performs automatic rolling update by creating new ReplicaSet.
* You can check the deployment status and rollback to previous version if something goes wrong.

### DaemonSet
* Cluster admins setup DaemonSet usually.
* for logs collections, node metrics DaemonSet is useful.
* it make sure pod runs on each and every node.
* If you join new node to the cluster DaemonSet make sure new pod comes up in the node similarly it will delete the pod when node goes down.

### StatefulSet
* Deployment is for stateless applications like backend api, frontend
* Deployment will not work stateful applications like MySQL, ELK, Redis, RabbitMQ.
* Deployments can have normal services, but stateful set should have headless service.
* Each pod will have its own storage in StatefulSet.
* Stable unique identifiers like pod name.
* If you kill the pod, another pod with same name will be created and attached to that storage.

### What is headless service?
If the ClusterIP field in None in the service, it is called as headless service. For normal service if you do nslookup it return the ClusterIP. But for headless service it returns all the IP associated with it.

For normal service the output is as below. It returns the cluster IP.

![alt text](service.PNG)

For headless service it returns the IP associated with it.

![alt text](headless-service.PNG)

### Why we need headless service to StatefulSet?

DB architecture is as below.

![alt text](storageApp.jpg)

* There will be one master which is able to read&write to the DB.
* Others are only for read queries.
* When master get write request it writes to its storage. It is master responsivity to communicate the changes with replicas. For that it has to do 2 things
    * Find out the replicas? How?
        * Master does the nslookp to get the replicas IP attached to headless service.
    * communicate the changes.
        * Master communicate changes to each pod through IP.

