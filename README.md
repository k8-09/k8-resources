### Flow of development

![alt text](dev-flow.jpg)

### Kubectl Commands

By default, you should have your kubernetes configuration exist in the home folder
```
~/.kube/config
```

to get the nodes
```
kubectl get nodes
```
to apply manifest
```
kubectl apply -f <name-of-file>
```
to delete 
```
kubectl delete -f <name-of-file>
```
to login to pod
```
kubectl exec -it <pod-name> -- bash
```
to login to pod with multiple containers
```
kubectl exec -it <pod-name> -c <container-name> -- bash
```
to get pods
```
kubectl get pods
```

Our Robo Shop K8 architecture

![alt text](k8-roboshop.jpg)